
#include "BioEnemyShip.h"


BioEnemyShip::BioEnemyShip()
{
	SetSpeed(150);
	SetMaxHitPoints(1);
	SetCollisionRadius(20);
}


// Method to update BioEnemyShip
void BioEnemyShip::Update(const GameTime *pGameTime)
{
	// When active
	if (IsActive())
	{
		// Using sin to get the x position?
		float x = sin(pGameTime->GetTotalTime() * Math::PI + GetIndex());
		x *= GetSpeed() * pGameTime->GetTimeElapsed() * 1.4f;
		// Moving positions for enemy ship
		TranslatePosition(x, GetSpeed() * pGameTime->GetTimeElapsed());


		// Deactivate ones ship is shot and/or off screen?
		if (!IsOnScreen()) Deactivate();
	}

	// Updates enemy ships
	EnemyShip::Update(pGameTime);
}


void BioEnemyShip::Draw(SpriteBatch *pSpriteBatch)
{
	if (IsActive())
	{
		pSpriteBatch->Draw(m_pTexture, GetPosition(), Color::White, m_pTexture->GetCenter(), Vector2::ONE, Math::PI, 1);
	}
}
